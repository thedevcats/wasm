const fs = require("fs");
const c_source = fs.readFileSync("./assets/wasm/fib_c_old.wasm");
const { fib_js } = require("./util/fib_js");

const env = {
  memoryBase: 0,
  tableBase: 0,
  memory: new WebAssembly.Memory({
    initial: 256,
  }),
  table: new WebAssembly.Table({
    initial: 0,
    element: "anyfunc",
  }),
};

var c_typedArray = new Uint8Array(c_source);

function useC() {
  WebAssembly.instantiate(c_typedArray, {
    env: env,
  })
    .then((result) => {
      console.time("Fibonacci using C Language");
      result.instance.exports.fib(42);
      console.timeEnd("Fibonacci using C Language");
    })
    .catch((e) => {
      // error caught
      console.log(e);
    });
}

function useJs() {
  console.time("Fibonacci using Javascript Language");
  fib_js(42);
  console.timeEnd("Fibonacci using Javascript Language");
}

(() => {
  useC();
  useJs();
})();
