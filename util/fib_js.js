function fib_js(n) {
  if (n <= 1) {
    return 1;
  }
  return fib_js(n - 1) + fib_js(n - 2);
}

module.exports = {
  fib_js: fib_js,
};
